SE - PHP - Practical

Email : udeepharsha@gmail.com

Used Component And Techniques

1. Laravel 5.3
2. Angular js
3. Rest Api
4. Bootstrap
5. Composer


**** 
Please install the database from sql file which located in (root_folder/database/se_basic_db.sql)

****
migration files are not here.so import the database

Thats it..


You can now access the data from gui..
********************************************************


ex: http://localhost/se_basic_app/public/students
	http://localhost/se_basic_app/public/grades
	http://localhost/se_basic_app/public/rooms

**room = class


relations are here..so must fill right ids for data sumitting..no validations are implement..

**************************
ex: If you fill room id you must insert exist room id,othervise will get an error..


You can now access the data from rest api..
********************************************************

ex: http://localhost/se_basic_app/public/api/v1/

Route::get('/api/v1/grades/{id?}', 'Grades@index');
Route::post('/api/v1/grades', 'Grades@store');
Route::post('/api/v1/grades/{id}', 'Grades@update');
Route::delete('/api/v1/grades/{id}', 'Grades@destroy');

Route::get('/api/v1/rooms/{id?}', 'Rooms@index');
Route::post('/api/v1/rooms', 'Rooms@store');
Route::post('/api/v1/rooms/{id}', 'Rooms@update');
Route::delete('/api/v1/rooms/{id}', 'Rooms@destroy');

Route::get('/api/v1/students/{id?}', 'Students@index');
Route::post('/api/v1/students', 'Students@store');
Route::post('/api/v1/students/{id}', 'Students@update');
Route::delete('/api/v1/students/{id}', 'Students@destroy');


********************************

database table are created in innodb engine so foreign_key contraints are working.

