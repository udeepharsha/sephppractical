<?php

namespace App\Http\Controllers;

use App\Room;
use App\Grade;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Rooms extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {
        if ($id == null) {
            return Room::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        $room = new Room;
        $room->name = $request->input('name');
        $grade_id = $request->input('grade_id');
        $grade = Grade::find($grade_id);
        $room->grade()->associate($grade);
        
        $room->save();

        return 'Room record successfully created with id ' . $room->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Room::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $room = Room::find($id);

        $room->name = $request->input('name');
        $grade_id = $request->input('grade_id');
        $grade = Grade::find($grade_id);
        $room->grade()->associate($grade);

        $room->save();

        return "Sucess updating Room #" . $room->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $room = Room::find($id);
        $room->delete();
        
        return "Room record successfully deleted #" . $id;
    }
}
