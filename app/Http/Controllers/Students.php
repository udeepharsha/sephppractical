<?php

namespace App\Http\Controllers;

use App\Student;
use App\Room;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Students extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id = null) {
        if ($id == null) {
            return Student::orderBy('id', 'asc')->get();
        } else {
            return $this->show($id);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        $student = new Student;
        $student->name = $request->input('name');
        $student->email = $request->input('email');
        $room_id = $request->input('room_id');
        $room = Room::find($room_id);
        $student->room()->associate($room);

        $student->save();

        return 'Student record successfully created with id ' . $student->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        return Student::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        $student = Student::find($id);
        $student->name = $request->input('name');
        $student->email = $request->input('email');
        $room_id = $request->input('room_id');
        $room = Room::find($room_id);
        $student->room()->associate($room);

        $student->save();

        return "Sucess updating Student #" . $student->id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $student = Student::find($id);
        $student->delete();
        
        return "Student record successfully deleted #" . $id;
    }


    /**
     * List of students in a specific class.
     *
     * @param  int  $id
     * @return Response
     */

    public function studentsList($id) {
        $students = Student::where('room_id', '=', $id);
        return $students; 
    }
}
