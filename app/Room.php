<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
   public function grade()
   {
        return $this->belongsTo('App\Grade');
   }

   public function student()
   {
        return $this->hasMany('App\Student');
   }
}
