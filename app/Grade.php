<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
   public function room()
   {
        return $this->hasMany('App\Room');
   }
}
