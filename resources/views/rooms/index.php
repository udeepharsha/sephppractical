<!DOCTYPE html>
<html lang="en-US" ng-app="roomsRecords">
    <head>
        <title>SE-PHP-Practical</title>

        <!-- Load Bootstrap CSS -->
        <link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">
    </head>
    <body>
        <div ng-controller="roomsController">
            <div class="container">
                <h2>School Rooms</h2>

                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Grade Id</th>
                            <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Room</button></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="room in rooms">
                            <td>{{  room.id }}</td>
                            <td>{{ room.name }}</td>
                            <td>{{ room.grade_id }}</td>
                            <td>
                                <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', room.id)">Edit</button>
                                <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(room.id)">Delete</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- End of Table-to-load-the-data Part -->
            </div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                        </div>
                        <div class="modal-body">
                            <form name="frmrooms" class="form-horizontal" novalidate="">

                                <div class="form-group error">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="name" name="name" placeholder="room name" value="{{name}}" 
                                        ng-model="room.name" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmrooms.name.$invalid && frmrooms.name.$touched">Name field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Grade Id</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="grade_id" name="grade_id" placeholder="grade id" value="{{grade_id}}" 
                                        ng-model="room.grade_id" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmrooms.grade_id.$invalid && frmrooms.name.$touched">Grade field is required</span>
                                    </div>
                                </div>


                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)" ng-disabled="frmEmployees.$invalid">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
        <script src="<?= asset('app/lib/angular/angular.min.js') ?>"></script>
        <script src="<?= asset('js/jquery.min.js') ?>"></script>
        <script src="<?= asset('js/bootstrap.min.js') ?>"></script>
        
        <!-- AngularJS Application Scripts -->
        <script src="<?= asset('app/app_cls.js') ?>"></script>
        <script src="<?= asset('app/controllers/rooms.js') ?>"></script>
    </body>
</html>