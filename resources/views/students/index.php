<!DOCTYPE html>
<html lang="en-US" ng-app="studentsRecords">
    <head>
        <title>SE-PHP-Practical</title>

        <!-- Load Bootstrap CSS -->
        <link href="<?= asset('css/bootstrap.min.css') ?>" rel="stylesheet">
    </head>
    <body>
        <div ng-controller="studentsController">
            <div class="container">
                <h2>Students</h2>

                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Room</th>

                            <th><button id="btn-add" class="btn btn-primary btn-xs" ng-click="toggle('add', 0)">Add New Student</button></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="student in students">
                            <td>{{  student.id }}</td>
                            <td>{{ student.name }}</td>
                            <td>{{ student.email }}</td>
                            <td>{{ student.room_id }}</td>
                            <td>
                                <button class="btn btn-default btn-xs btn-detail" ng-click="toggle('edit', student.id)">Edit</button>
                                <button class="btn btn-danger btn-xs btn-delete" ng-click="confirmDelete(student.id)">Delete</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- End of Table-to-load-the-data Part -->
            </div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">{{form_title}}</h4>
                        </div>
                        <div class="modal-body">
                            <form name="frmstudents" class="form-horizontal" novalidate="">

                                <div class="form-group error">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control has-error" id="name" name="name" placeholder="student name" value="{{name}}" 
                                        ng-model="student.name" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmstudents.name.$invalid && frmstudents.name.$touched">Name field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control has-error" id="email" name="email" placeholder="email adress" value="{{email}}" 
                                        ng-model="student.email" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmstudents.email.$invalid && frmstudents.email.$touched">Email field is required</span>
                                    </div>
                                </div>

                                <div class="form-group error">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Room Id</label>
                                    <div class="col-sm-9">
                                        <input type="test" class="form-control has-error" id="room_id" name="room_id" placeholder="room id" value="{{room_id}}" 
                                        ng-model="student.room_id" ng-required="true">
                                        <span class="help-inline" 
                                        ng-show="frmstudents.room_id.$invalid && frmstudents.room_id.$touched">room id field is required</span>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" ng-click="save(modalstate, id)" ng-disabled="frmstudents.$invalid">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Load Javascript Libraries (AngularJS, JQuery, Bootstrap) -->
        <script src="<?= asset('app/lib/angular/angular.min.js') ?>"></script>
        <script src="<?= asset('js/jquery.min.js') ?>"></script>
        <script src="<?= asset('js/bootstrap.min.js') ?>"></script>
        
        <!-- AngularJS Application Scripts -->
        <script src="<?= asset('app/app_student.js') ?>"></script>
        <script src="<?= asset('app/controllers/students.js') ?>"></script>
    </body>
</html>