<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/grade', function () {
    return view('grades/index');
});

Route::get('/rooms', function () {
    return view('rooms/index');
});

Route::get('/students', function () {
    return view('students/index');
});


Route::get('/api/v1/grades/{id?}', 'Grades@index');
Route::post('/api/v1/grades', 'Grades@store');
Route::post('/api/v1/grades/{id}', 'Grades@update');
Route::delete('/api/v1/grades/{id}', 'Grades@destroy');

Route::get('/api/v1/rooms/{id?}', 'Rooms@index');
Route::post('/api/v1/rooms', 'Rooms@store');
Route::post('/api/v1/rooms/{id}', 'Rooms@update');
Route::delete('/api/v1/rooms/{id}', 'Rooms@destroy');

Route::get('/api/v1/students/{id?}', 'Students@index');
Route::post('/api/v1/students', 'Students@store');
Route::post('/api/v1/students/{id}', 'Students@update');
Route::delete('/api/v1/students/{id}', 'Students@destroy');
